+++
framed = true
+++

**Hi everybody and welcome to this space!**

I've created this to share what I do on a daily basis (mostly write-up).
I'm doing from time to time challenges from CTF, so I can share this here with you.
I also like pentesting so you can find some reports I did from boxes on HackTheBox/TryHackMe for example.    
Finally I enjoy putting some effort in my personnal work so you might find some of my projects here too!

Hope to see you again and of course have fun reading my stuff here ;)
