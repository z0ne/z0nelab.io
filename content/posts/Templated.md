+++
title = "Templated"
date = "2022-05-30T11:56:43-04:00"
author = "z0ne"
tags = ["HTB", "Challenges", "SSTI"]
+++

# Intro

I wanted to make a web challenge and I never try this section on HTB, so let's start!

First we go in the web challenge section and launch it like this:

![chall](/templated_0.png)

We got an `ip:port` to connect to and the description is `Can you exploit this simple mistake?`, so let's go.

# Recon on Services

First before accessing the site, I just wanted to check that there isn't any flaws related to the services running on the server, so we're going to use `nmap` like this: `nmap -sC -sV -Pn 188.166.172.138` (the first try told me that the server wasn't up so I had to use the option `-Pn` to skip host discovery).

![nmap_1](/templated_1.png)

No ports/services open it seems, okay let's check what's running on the port `32417` (the one hosting the website) by adding the option `-p 32417`:

![nmap_2](/templated_2.png)

Okay we have a service called `Werkzeug` running on version `1.0.1` with Python `3.9.0`.

If we check online we find that `Werkzeug` is a library used primarly by `Flask` which make sense since the port `32417` host a webserver. I found an exploit related to `Werkzeug` but our version is a patched one so it's a dead end.

Even though the exploit won't work for our version I found an **INCREDIBLE** article talking about it (even though it's pretty old it's still good to know I guess!). What makes this article cool from my perspective is that like `Log4Shell`, the issue wasn't linked to a particular tool or software but a library used by websites/softwares/tools around the world. It's exactly what happen with the `Werkzeug Debug Shell` vulnerability. It's not specific to `Flask` but every software, website that use Webzeuk and have `WERKZEUG_DEBUG_PIN=off` is **VULNERABLE**! Of course the vulnerability in itself is quite old and didn't have an impact as loud as `Log4Shell` but still it's good to know that it's not really the first time this type of thing happen! 

You can find the post just right [here](http://ghostlulz.com/flask-rce-debug-mode/) and I also give you the link to check and potentially exploit this vulnerability [here](https://www.exploit-db.com/exploits/43905)!

# Web Part now!

Now that it is put aside, with a simple web browser we can check what the site looks like:

![index](/templated_3.png)

And from this result, well we don't have anything to exploit, let's dig then.

We got at least an info, this site is powered with `Flask/Jinja2`.

`Flask` is a micro web framework written in Python and `Jinja` is a web template engine for the Python programming language.

Since we're on the Home Page, we should use dirb to find other directories. We're going to do so like this: `dirb http://188.166.172.138:32417/ /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -f`

This command gave us that output:

![dirb](/templated_4.png)

...which is rather odd.

If we reproduce this request on our browser, we got a 404 that looks like this:

![404_1](/templated_5.png)

Now I think we found our way in. First thing first, `%20` is url encoded, so when it's print on a web browser it's literally a space like this: ` `.

And it's exactly what we're seeing on the screenshot above. We could check again with the second result we got from dirb, it looks like this:

![404_2](/templated_6.png)

We can clearly see now that through our `GET` request we can print and modify the HTML file.

I already used `Flask/Jinja` during my `CS50x` (you can check my post right [here](https://z0ne.gitlab.io/posts/cs50x/)) so I know some features of it. Jinja is a template engine, you can print a string you obtained from a `GET` request that a client has sent you and that's exactly what I think is happenning. For the error handling during the return of a 404 code, the coder gave us the power to manipulate the string we send to the server and there will be no measures of security to check the input (in a common usage it's supposed to be a page we ask through our `GET` request but I'm going to say string for the "page" we request from now on). So what this type of problem can lead to ? Well an injection through the template engine. Even though I already use `Jinja` I didn't use it in a "red team way" so I don't know how to do this with it, so I might simply just google a thing like `jinja injection` to see if we find something:

![jinja_injection](/templated_7.png)

And the first result immediatly gave us a techique that will help us. It's called a Server Side Template Injection (commonly known as an `SSTI`) and of course through Jinja it's possible. 

# So what is it ? 

*Template injection allows an attacker to include template code into an existing (or not) template. A template engine makes designing HTML pages easier by using static template files which at runtime replaces variables/placeholders with actual values in the HTML pages* 

I should add that in our case it's a server side Template Injection since we're sending a string to the server and it print what we send on the HTML file.

The next step will be to check how to exploit this type of attack, to do so, we will use one of the **BEST** repo of Github for Red Team and various offensive operations, `PayloadsAllTheThings`.

If you don't know what it is, you can check just right [there](https://github.com/swisskyrepo/PayloadsAllTheThings)

If you do know what it is, long story short I found the README with all the Server Side Template Injection techniques and I also found the `Jinja` part of it, you can check right [there](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Server%20Side%20Template%20Injection/README.md#jinja2) all the payloads (and especially the one related to `Jinja`).

For our exploitation, we'll use this category:

![SSTI_payload](/templated_8.png)

Our environnement use `Jinja2` so we meet all the requirements, let's start with the exploitation phase!

# Exploitation

Ok let's remind us what we want to do, first this is a challenge asking us a flag to validate, to get this flag we need to access a file probably called `flag.txt`, to do so, we have to go with a `SSTI`. To exploit, this type of technique lead us obviously to a `RCE` type of attack (we could go with a reverse shell, but it's just simpler, faster and more reliable to just make a `RCE` to get our flag.)

To do so we go into the sub category of `Jinja` `SSTI` to check what payload let's us execute remote commands.

This one seems interesting: `{{ self._TemplateReference__context.cycler.__init__.__globals__.os.popen('id').read() }}`

Through the call of `os.popen().read()` (`jinja` is python based remember?) we can execute commands remotely.

We're going to test the payload with the `id` command and to do so, we need to put the payload just after the `ip:port`, the payload at the end will look like this: `http://188.166.172.138:32417/{{ self._TemplateReference__context.cycler.__init__.__globals__.os.popen('id').read() }}`. This time the output will be the result of our command and not some random string we input in our `GET` request.

It worked!!! :

![SSTI_id](/templated_9.png)

Ok, now let's see what inside our current directory with `ls`:

![SSTI_ls](/templated_10.png)

Again just to get everyone on the same page, our output is between `''` since it's there that `Jinja` has been programmed to print what page the user requested (in our case we modify the usage of this feature since we input a command but I'm sure you get what a `RCE` is and roughly how Jinja works now :) )

Last thing through the output we see a `flag.txt` file that seems to be in the root directory (`/`).

Let's check the content with `cat`:

![SSTI_cat](/templated_11.png)

Finally we got our flag! : `HTB{t3mpl4t3s_4r3_m0r3_p0w3rfu1_th4n_u_th1nk!}`

Last thing, the payload can be shortened like this (we can see this in `PayloadAllTheThings`):

![SSTI_shortened](/templated_12.png)

If you want more info, on how the payload is craft, shortened you can check the article just [here](https://podalirius.net/en/articles/python-vulnerabilities-code-execution-in-jinja-templates/) thanks to [@podalirius_](https://twitter.com/podalirius_)

And of course this new short payload works like a charm:

`id` command:

![SSTI_shortened_id](/templated_13.png)

`flag`:

![SSTI_shortened_flag](/templated_14.png)

I hope you like this post and stay tuned for more!

> Never trust the input I guess... ¯\\_(ツ)_/¯ 