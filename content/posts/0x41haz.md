+++
title = "0x41haz a.k.a Simple Reversing Challenge"
date = "2022-04-29T05:00:05-04:00"
author = "z0ne"
tags = ["THM", "Challenge", "Crackme"]
+++

**Introduction**

As you can see bellow, instructions are fairly simple, download and reverse the binary!

![find_the_password](/0x41haz_0.png)

The last thing that we can notice, there are "anti-reversing" mesures in place, good to know, now let's start!

(You can do the challenge just [here](https://tryhackme.com/room/0x41haz) if you want to follow)

**Analysis**

![ls](/0x41haz_1.png)

The first thing we see is a file with a `.0x41haz` extension (of course in linux it doesnt matter, so if we live it that way it's cool. Also there is no such thing as this type of extension in general, even in windows...)

Now we'll need to use the file command to see the type of architecture, executable etc...

The command looks like this: `file 0x41haz.0x41haz`

The result we got is like that. 

![find](/0x41haz_2.png)

We saw that the file is of ELF format. To put it simple, it's not an `.exe` (windows executable), the ELF format is linux/unix specific).

Last thing, we got something weird, the `*unknown arch 0x3e00*` message, seems to be some kind of error.

We try to make the file executable like this: `chmod +x 0x41haz.0x41haz` just to check what happen when we execute the program.

![exec](/0x41haz_3.png)

We can see a lil bit about how the program works, we try the letter `A` but of course it doesnt work.

Check with the program `strings` all the strings in the file, like this: `strings 0x41haz.0x41haz`

![strings](/0x41haz_4.png)

Just above you can see a part of the output, the password isn't stored in the source code itself but the line just after `Well Done!!`, is pretty weird: `;*3$"`.

Now we use `r2` to debug and do a dynamic analysis of the file.

![r2](/0x41haz_5.png)

But wait it's weird, we can't analyze the file and list the functions!

Well, it might be the 'measures of protection' that not let us reverse the binary.

Let's go back in time, when I used the `file` command I got an error.

I'm going to google it this error since I dont fully understand what's going on.

![google_it](/0x41haz_6.png)

When I click on the first link and browse a lil bit (thanks to this [link](https://pentester.blog/?p=247) and of course the author, a.k.a Patrice Siracusa)

I found this! :

![trick](/0x41haz_7.png)

The first information about the 5th byte is not useful for us, I tried changing 64 to 32 bit and we can't reverse.

![64_to_32](/0x41haz_8.png)

So we're going to change the 6th byte, let's try changing endianness then !

We start with the command `hexeditor 0x41haz.0x41haz` and got this:

(Before)
![hexeditor_before](/0x41haz_9.png)

(After)
![hexeditor_after](/0x41haz_10.png)


We have edited the 6th byte and change it from 2 to 1!
(Basically, changing from Big-Endian to Little-Endian)

Now that we have done that, maybe we should check with the `file` command if it works:

![file_MSB](/0x41haz_11.png)

COOL we got something new!

Now let's try to debug with r2!

![r2_new](/0x41haz_12.png)

It worked!

After disassembling the main function we can see from the beginning that three variables are not only declared (compared to others) but initialized too (they hold value if you want).

![r2_disass_main](/0x41haz_13.png)

Those 3 are:

1. `var_16h` that holds `2@@25$gf`
2. `var_eh` that holds `sT&@`
3. `var_ah` that holds `L`


Even though assembly is pretty important to learn for reversing, I switch to ghidra, to get everyone on the same page (and to make the analysis "simpler" since we're going to get a C/C++ alternative compared to assembly)

After ghidra finished analyse the binary, we go into the entry function by searching through the symbol tree like that:

![symbol_tree](/0x41haz_14.png)

In the decompile section we go to the first function (`FUN_00101165`, present in the arguments of the function), to check if it's main!

![decompile_entry](/0x41haz_15.png)

We now click on `FUN_00101165` and we can finally see main!

![decompile_main](/0x41haz_16.png)

Luckily for us, it is!

First thing that's going to block us is this part of the program:

![first_barrier](/0x41haz_17.png)

The variable involved is `local_48` and is declared like this:

![local_48](/0x41haz_18.png)

The max length the variable can hold is `43`. But it's not important, what's important (and what we can see on this particular part of the program below), is that it check the length of the input (stored in the `local_48` variable) and if it's not equal to `13` (`0xd` in hexadecimal), the program output `Is it correct , I don\'t think so.` and quit with return code equal to `0` through the call of the function `exit()`.

From this if condition, we can assume that our flag has a length of `13`!

By executing the binary again with `13` `A`, we clearly see that we're on the right way since the message is different this time!

![exec_13_A](/0x41haz_19.png)

The two other conditions that block us are those one:

![two_last](/0x41haz_20.png)

The first condition print out `Well Done !!` if `local_c` (the variable initialized before the while loop at `0`) is equal to `12` (`0xc` in hexadecimal).

But why this difference from `0xd` to `0xc` ? Well `12` is the string length itself, without the `null character` a.k.a `\0`, so if we count it it's `13`, but again what we're comparing is all the char involved in the string not the `null character`.

The second condition break (and output `Nope`, like the one we had earlier when executing the program with `13` `A`) if a char from the string `local_le` is not equal to a char of the input we stored in `local_48`.

Just to make it clear, the program use `local_c` to switch to the next character when comparing. From the `local_le` side we use the address of this variable (through the `&` operator) to get this value `2@@25$gf` (`var_16h` in r2 is `local_le` in ghidra) and on the other side the `local_48` will let us compare each char of the string we input. But again, the flag is suppose to be `13` (or `12` without the null operator) chars in length. 

Since there are two more variables that follow up after `local_le` (`var_eh` in r2 is `local_16` in ghidra and `var_ah` in r2 is `local_12` in ghidra). We use `local_c` to add the other two addresses, what I mean is that after some point, when `local_le` is passed, we get `local_16` and `local_12` in place to be compared in the if condition through the incrementation of `local_c`. It's going to move up to the next memory address after each incrementation, starting with the address of `local_le`. But as we can remember we don't access directly the value hold by these variables but their memory addresses that contain those values through the `&` operator. Finally the `local_e` side when it's doing this if condition, transform the value we hold through the pointer into a string with a type conversion that looks like this: `(char *)`, We can therefore only assume that our flag at the end is a concatenation of the three variable we list earlier, `local_le`, `local_16` and `local_12`).

Just to put this as simple as that, since we changed from `r2` to `ghidra`, those values are the same, they just don't use the same name between the two platforms,

So if I do a simple recap:

1. `local_le` holds `2@@25$gf`
2. `local_16` holds `sT&@`
3. `local_12` holds `L`

If we do some concatenation, we have that: `2@@25$gfsT&@L`.

We can check the length quickly through bash:

![bash_check_length](/0x41haz_21.png)

And bingo it's a length of 13!

Now let's check into the program if it works!

![flag](/0x41haz_22.png)

Finally we got our flag!

Let's check into TryHackMe if it's good,

![THM_question](/0x41haz_23.png)

From this format I will probably put the flag in a format like this: `THM{2@@25$gfsT&@L}`

![Final_check](/0x41haz_24.png)

Finally! The flag we got was good!

I hope you liked this simple reverse challenge analysis and wish you a good day (or night) ! 

> Non est ad astra mollis e terris via


