+++
title = "CS50x recap or how my first course in CS went"
date = "2022-01-15"
author = "z0ne"
tags = ["CS50x", "Dev", "Fundamentals"]
+++

**Ok before continuing reading this post I need to make few disclaimers**:
- First off, no solutions here, I respect the academic honesty policy of the course.
- Second thing, this is not a post with every details on each labs/problems/lectures etc, It's just some regular post and my opinion after a few months (precisely 6 since I saw on github my last commit was on july), so it's not really fresh in my memory I won't be very precise about everything I did, I might omit some infos for example... So if you search something with more infos or details I'm sure someone else made things more precise for you online!
- Last but not least, this post is really just about what we did, so it's an introduction per say to the overview of the course + my opinion sometimes on this one, again it's personnal and also apart from when I'm giving my opinion, this post is meant for people who don't really know the course or what we actually do in it.

**Ok, let's start!**

# What's CS50x?

It's an entry-level course taught by David J. Malan, CS50x teaches students how to think algorithmically and solve problems efficiently. Topics include abstraction, algorithms, data structures, encapsulation, resource management, security, software engineering, and web development.

[CS50x Official website](https://cs50.harvard.edu/x/)

# How it went?

Well pretty good for me, I was a complete beginner for all the things related to software development and even if I did some minor projects before, I had at the time some problems with the logic I had when building programs (What I mean is that there was better way to achieve the goals I had and somehow at the time, I always picked the worst way to achieve it) or even little issues that I couldn't solve since I wasn't that good for developing a project. So I really wanted to be better at the art of programming :) I searched for a good course to really work this out and I find CS50x! The course have 10 lectures if I'm correct (one each week but you can take more than a week, since it's an online courses).
At the end of each lecture you have two things to complete, labs (exercices to apply the things you've learned) and problem sets to evaluate how you apply what you learn. Compared to labs you don't have solutions and you have to find what you can do on your own. Both are graded and if I'm not wrong, you need at least 70% in each week or something like that of completion to pass the course (I made 100%).

# So what's in this course?

#### WEEK 0 - Scratch / Week 1 - C

**A LOT OF THINGS**, you start with scratch (a high-level block-based visual programming language), to learn all of the basics in programming, variables, conditions, loops etc...
From there you'll dig in with real stuff, you learn C and for the first time you really program by translating ideas you learned with scratch. For example the block if then in scratch:

![if_then_scratch](/cs50x_0.png)

 is in C :
``if (condition) {}``

#### Week 2 - Arrays

From this type of thinking, you begin to really develop your sense of logic and think in a programmed way, so to speak. After you learned the basics, you level up a lil bit.
You start with arrays, functions, variables and their types in C and so much more like the scope of variables in functions and command line argument! From the challenge (or the two problem sets to be precise) that I remembered from this week, It was linked somehow to the cybersecurity field since we had to do some cryptography! We had first a caesar challenge with options (through command line argument) like this:

```
$ ./caesar 13
plaintext:  HELLO
ciphertext: URYYB
```

to know what key we're going to use (in this case it's ROT13 since the argument is 13).

The second challenge was pretty similar It was called subsitution and the goal was again to use command line argument to get the key. It look like this:

```
$ ./substitution JTREKYAVOGDXPSNCUIZLFBMWHQ
plaintext:  HELLO
ciphertext: VKXXN
```

Compared to caesar it was a little bit different, because the key is not used to rotate position for each letter of the input but this time it's actually an entire alphabet (so a string of 26 chars) you give to the program. It can then replace it to produce a ciphertext by replacing the letter on the actual position of the alphabet by the letter on the same position on the key you gave. (like if your input is ABC and your key is XYZ then the ciphered version of this plaintext would be XYZ).

#### Week 3 - Algorithms

After covering the basics and + in C, that's when the course start to be really intersting (and also increase in complexity) because you go really in the deep core of what make software development an elegent thing, it was the week course for algorithms!
So we really learned a lot of things like sorting algorithms (bubble, selection and merge sort). From that on since we learned sorting elements, we could searched them for the value we wanted, so we start learning Linear/Binary search! Finally we got also into recursion to apply thoses algorithms!

Difference between Linear ( O(n) )  and Binary ( O(log n) ) search as an example:

![search_algorithms](/cs50x_1.png)


#### Week 4 - Memory

Since we already did, some basics + algorithms the next step is memory (we learn C so why not learn fundamentals like memory managment etc). So we learned, pointers, dynamic memory allocation, file pointers etc. For this particular week (the 4th), I remember a challenge so cool since it could be apply to a real case scenario in cybersecurity. The challenge headline was: *Implement a program that recovers JPEGs from a forensic image, per the below.* 

```$ ./recover card.raw```

You got a raw file and you had to make a program to retrieve those jpegs based on [magic number](https://en.wikipedia.org/wiki/Magic_number_(programming)) but also [memory block](https://en.wikipedia.org/wiki/Block_(data_storage))


#### Week 5 - Data Structures

This week like the two previous was pretty tough since we really dive into "abstract" things. In a certain way we could compare this week to the third with algorithms since we studied subject with a really good approch but it was still really abstract in sort of. Ok, so this week was used to learn data structures, we got into linked lists (singly-linked lists to be precise), hash tables and even tries! The problem set was some sort of a speller check that spell checks text files. It's using hash table to work! Just to share a lil bit more about data structures, here's a scheme of how a singly-linked list worked (for those who never did things like that), so singly-linked list goes through elements each of which contains the content of the current element + the pointer to the next element (or NULL if it's the last one), you can see a clear scheme here!:

![singly-linked_list](/cs50x_2.png)

#### Week 6 - Python

During this week we really restart the process of learning a new language. During the lecture we did the same thing as we did with C and scratch like translating ideas we learn on scratch to the actual way to do it in C but this time, it was related to python so we learned how to do the basics stuff from C to python. Furthermore, to really help us getting familiar with python we rewrite some of the programs we did in C in early weeks but this time in python, it was the same problem (even if we had one new, but the most part was problem sets we already knew) but just with a different language! Regarding to the only new problem set we got (DNA) the headline was: *Implement a program that identifies a person based on their DNA, per the below.*

```
$ python dna.py databases/large.csv sequences/5.txt
Lavender
```

Before going in more details, the execution is pretty simple we have two files in argument, the first one is from the databases folder and contain a CSV file (comma separated value) who contain names associated to their respective STRs. This could look like this in its simplest form:

```
name,AGAT,AATG,TATC
Alice,28,42,14
Bob,17,22,19
Charlie,36,18,25
```
(An STR is a short sequence of DNA bases that tends to repeat consecutively numerous times at specific locations inside of a person’s DNA. The number of times any particular STR repeats varies a lot among individuals.) The probability that two person have a STR in common is 5%, the more STRs we compared, more chanve we got to ID the person's sequence. For example the FBI use 20 STRs for their DNA database, in this pset we used 8 of them. 

 The second file is in the sequence folder and contain unknown sequences that you need to identify by calculating how many times a particular STR occures to then compared it to our databse!

 This problem was really cool since I discovered the process to identify a person based on their DNA!


#### Week 7 - SQL

For this week we learned a new language, SQL! With this lecture we learned to query databases and interact with them (CRUD a.k.a Create, Read, Update, Delete). Right below a scheme to show sample queries we used to make these operations:

![CRUD](/cs50x_3.png)

 After the lecture and labs, again one of the PSET was SO FUN. We had to make an investigation to find a thief, we got a db file and a lot of tables in it to query. For example we could query the table with all flights and passengers to try to find who left the city ater the robbery and things like that. So through JOINS we could crossed query to get data so we would find the perpetrator and the accomplice of this!

#### Week 8 - HTML, CSS, JavaScript

This week was full of surprise, since we got an introduction to a lot of subjects, from the basics of Internet, like IP/TCP/HTTP (love retaking course on networking basics !), to HTML, CSS, Javascript and even some more advanced topics (we did a lil bit of DOM).
So yes as you can see compared to other weeks this one was really full of new topics! It's not even over! The problem set for this week was a personnal project you had to do, by creating your own site with html/css/js with a lil bit of DOM ! So again compared to other weeks where it's much more strict regarding the rules to pass the problem set, this one was really special since it was our project! For my personnal project I created some trivia + homemade wiki. The topics of this trivia was Harvard and Yale, so you got specific question like when it was created, where it is etc and if you didn't have a good enough grade you were redirected onto a homemade wiki I did with the answers! Finally to close this, I like maybe to put a scheme of how DOM works since it's the "advanced" topic of this week and it was mainly used to do dynamic pages with Javascript!: 

![DOM](/cs50x_4.png)

#### Week 9 - Flask

This was in sort of our final week (from a technial perspective), and it was pretty much a mix,  with all the web technos we used, the lecture make us learn a new framework, flask!
But we also learned a web template engine (jinja) Those two where used to build this amazing website that you see just below:

![Finance](/cs50x_5.png)

This website was used to access stocks price, bought them, selled them and all of that realted to an account we had. They were a lot of things to get done like updating our account, getting the good price for the stocks and the difference we made (like do we made a profit when we selled etc...) 
We used sql and db file to build and store the model with all the data and the instructions for each CRUD operations etc... 
We used HTML/CSS and Javascript if I'm correct to render our beautiful view,
Finally we used python, or I should say flask which is a python framework to have the last thing, our controller and really do the operation on the database or when do we have to call the view!
So yup for this project we had a MVC pattern:

![MVC](/cs50x_6.png)

It was really fun to really complete the final problem set that used all the learning we had from the past few weeks, also just as a disclaimer we had to build most of a site but still we didn't build that from scratch the bones of the sites were proprosed directly for the problem set and we had to build on top of that! But again the mix of all the things we've done before was really insane to do !

#### Week 10 - Ethics

The last week was one of the most interesting, since it allowed us to discover two uniques things, or it's better to say, to learn two unique things. The first one was an introduction/tiny lecture on the subject of "ethics" taught by two professors (Meica Magnani and  Susan Kennedy) of the philosphy department. Besides all the technical parts I learned, I really appreciate this lecture since they gave us tools to really think more thoughtfully about what  we could type. For example, just because I can code something, would that mean that I should do it and if so, how should I do it ? It's really all about ethics, the "how should I do it" is not at all about learning technical things but it's more related to some main topics of philosophy like good and evil... I really thought it was indeed very great to raise ethical issues in the industry and how to be really aware of what you're doing and if what you're doing is the best thing you could do. The second unique things we had to do was the start of our final projects! Since we finished all of the lectures, problem sets, it was time for us to start our own project. There wasn't any limition or things to do precisely, it was something that you want to do, in a language you choose and in an area you choose.

My final project was a bash script, that was used to insure the secrecy of the data a user wants to protect, it is available on my gitlab just there!:

[My final project](https://gitlab.com/z0ne/crypt_cs50x)

In conclusion, I would say that this course really help me out to boost my skills, be better at developping software and also discover new fields and usage for the technologies I learned. Again this is only my opinion but I guess that for me if you really search a beginner, decent and reliable CS course, I can assure you from my own experience that I would recommand CS50 at 100%. I didn't said this is not going to be hard, even more for a complete beginner but the methodology, the things you could learn, that's really worth it, I can promise you. Thanks for reading this post and hope to see you again,

This was CS50 !

>  What ultimately matters in this course is not so much where you end up relative to your classmates but where you end up relative to yourself when you began ;)
Thanks to all the CS50 staff for this amazing journey !!!