+++
title = "Pandora - HTB report"
date = "2022-03-09T05:50:01-05:00"
author = "z0ne"
tags = ["HTB", "Red team", "CVE-2021-4034"]
+++

# Intro!

Hello everybody and welcome to this write-up. So even if I already did some boxes (primarly on THM but I also did some in HTB). I have more experience in the Challenges section for now on HTB compared to the number of boxes I did. But this blog will help me to  publish things I'll do in HTB! Anyway, now we should start.

# Information Gathering

First info will get is on what this box is running, regarding this screenshot below, it's a linux box.

![box_type](/pandora_0.png)

Since we can't really make more passive recon with a machine in the HTB network we will start directly with active recon! We're going to try to ping the machine to see if we get a response. As you can see below the machine seems to respond, so we won't have to use `-Pn` (Treat all hosts as online in nmap scan) and now we're 100% sure the box is up.

![ping](/pandora_1.png)

# Enumeration/Scanning

Next step would be to make an nmap scan, we got this as a result:

![nmap](/pandora_2.png)

Bingo ! We found that the machine host a website on port `80`, the port `22` will give us the possibility to ssh when we will have credentials for a user but for now it doesnt really give us any informations (it's always the same in HTB the port `22` is always open to `ssh` when we find something on another port).

When we go to `http://10.10.11.136` we found this:

![website](/pandora_3.png)

Unfortunatly at first glance and even with some digging we don't find anything on there. No file upload, no hidden directories, no options to get access to the backend just basic webpages, even with tools like gobuster, that doesn't give us anything.

After some thinking I came up with an idea, maybe we don't have the whole details of how this box operates? Why? Well my nmap scan only gives us two services (based on the default scan a.k.a `SYN Scan` more info [here](https://nmap.org/book/synscan.html)). Both of them are open and both of them are based on `TCP`, but we don't only have `TCP` based protocols, a lot of others are based on `UDP` so if I rerun a scan in nmap with a `-sU` option I'll get this: 

![nmap_UDP](/pandora_4.png)

Bingo ! We got something new, the port `161` open and the service is `SNMP`, it runs through net-snmp.

But what exactly is `SNMP`? Simple Network Management Protocol is a networking protocol used for the management and monitoring of network-connected devices in Internet Protocol networks. 

But still it's pretty vague right? Well there is more details, there are three key components for an SNMP-managed network: the `managed devices`, an `agent` (a software which runs on managed devices) and finally a Network managment station (`NMS`) software which runs on the `manager` (one or more administrative computers that manage the agents). 

`SNMP` operates in the application layer of the `TCP/IP` and all SNMP messages are transported via User Datagram Protocol (`UDP`).

The managed devices can be any type of device, including but not limited to, routers, access servers, switches, cable modems, bridges, hubs, IP telephones, IP video cameras, computer hosts and printers.

Last information to know the port open on our machine is `161`, but on the wiki page of there are two ports associated with `SNMP`, `161` and `162`. The first one is used by agents to receive requests from manager. The second one is used by managers to receive notifications (`Traps` and `InformRequests`) from the agents -> more info [here](https://en.wikipedia.org/wiki/Simple_Network_Management_Protocol). 

With this info we know that the machine is an `agent`, netstat tells us that the port in local is `161` and we send on any port not the `161` precisely in answer, we don't listen on `162` for `agents` notifications as if  we were a `manager`, so I conclude we run the `agent` part!

If we take a look at the screenshot we can see that I used the `-sC` option, it allow us to run the default scripts directory of nmap after the scan is done. In all of the lua scripts there are some scripts that lets us see some additionnals infos, but first let's see what scripts are interesting: 

![nmap_snmp_scripts](/pandora_5.png)

So only some scripts will be intersting, `snmp-info`, `snmp-interfaces`, `snmp-netstat`, `snmp-processes`, `snmp-sysdescr` and lastly `snmp-win32-software` (although the machine don't run on windows) If you want more info on the scripts located in the default directory of nmap you can check [here](https://nmap.org/nsedoc/categories/default.html).

Oh and a last note before looking at the outputs of these different scripts, I have to say that this could be done in 2 stage, like first one just scanning to see that `161` port is open and on the second one load only certain scripts (not the entire default directory of nmap like I did) to make less noise. We could also don't use nmap at all for the second phase and use something else like snmpenum (a perl script that enumerate information on machines that are running SNMP).

Now we're going to gather a lot of infos we got from this output.

First one is `snmp-info`, with this script the major information we got is the software used to deploy the protocols, in this case it's `net-snmp`.

![snmp-info](/pandora_6.png)


`snmp-interfaces` lets us look at the interfaces on the machine, it's connected through ethernet, we got the type (`VMware VMXNET3`) of the ethernet controller, the `MAC` address too (that confirms that the machine is powered through `VMware`).

![snmp-interfaces](/pandora_7.png)

This one is pretty long, but we got some interesting infos, first thing since this script allows us to query `SNMP` to get a `netstat` like output, we see all the ip, ports the machine is using to send/receive informations. With that no need to scan with nmap to find higher ports for example but it helps us also to validated all the ports found by nmap. We saw the `22, 80, 161`...
But we also notice other connections from other IP and other ports.
Sadly this machine is only setting up to be hacked alone there's no networks with other devices/machines, so in real world scenario, this script will help us to identify all ports that are used by the machine but also to do recon on others machines on the network since we can see who talks to who. But here the outgoing `TCP` connections we see in the beginning of the output, especially the ones that are going on port `9999` and `4444`, these are the other players, that are pentesting this machine! So in a context of blueteam netstat  or netstat output like can be quite useful to detect intrusion!

![snmp-netstat](/pandora_8.png)

What you can see just below is a part of the `snmp-processes` script output. This is by far the most useful script for this box, it enumerates process through `SNMP` on the machine, so we know everything about it, how the webeserver is configured, how it runs, what scripts are running currently...
So in this example we can see that yes its a webserver with an `apache2` process running, we also notice the `ssh` daemon that will allow us to gain foothold in the machine later, even a `mysql` daemon, which mean that there will be possible databases in there. We won't forget two things (the first being obvious) the machine runs indeed `net-snmp` and use `SNMP` since we have a `snmp` daemon. The second one is the `Polkit` daemon that is used to provide an organized way for non-privileged processes to cummunicate with privileged ones. But we don't really need to be interesting in anything related to Polkit for now but the presence of this particular daemon indicates us that `Polkit` is used in this machine.

![snmp-processes_daemon](/pandora_9.png)

THe two first processes found by the script are not very useful (`20001`, `20005`), it could be anything, some script by the admin to check the version of the kernel, the users who are connected etc or another player on the machine who checks who's logged in, the version of the kernel etc...

But what seems really interesting, are the three processes that follows the two first (`20022`, `20023`, `20024`), the  first one is used to launch a backup and the two others are used to compress all the files they found (first one being everything in the directory `pandora_console` which is itself in the webserver directory so this has to do with backing up the website or some parts of this website) to finally store in a backup directory of the user root. The second one is again a compression with tar (strangly `/tmp/tar` is used for the second one, maybe an attempt by another player to do some hijacking of relative path ?) and it is located in `/root/.backup/` but this time the backup is more precise since its doing a compression inside pandora console on the `AUTHORS` and `COPY` folders.

![snmp-processes_webserver_backup](/pandora_10.png)

The screenshot below, even if it wont help us, is pretty cool though, since they're multiple players that are doing this box, we can see their reverse shell on the listing of the snmp-processes output, the same guy is using a reverse shell in shell scripting and another one in php! So looking at processes when doing `forensics` and `IR` is pretty useful I guess!

![snmp-processes_revshell](/pandora_11.png)

With `snmp-sysdescr` (that lets us extract system information from an `SNMP` service), We can see a lot of useful infos, the OS (`Linux pandora`),  the version of the kernel (`5.4.0-91-generic`), on what distro the OS is based on (`Ubuntu`), the processor have multiple cores (the mention `SMP` that stands for Symmetric multiprocessing tells us that!) and finally the architecture (`x86_64`)! This script output is really useful since it helps us gathering infos about the system!

![snmp-sysdescr](/pandora_12.png)

Here through the `snmp-win32-software` (even if its not a machine running windows) script output it let us know every software installed! The list is too long to screenshot the entire output but just know that this script can be very useful to see what's installed on the machine! 

![snmp-win32-software](/pandora_13.png)

The last and most important info (collected by going back, but I wanted you to see every script we could used before going to the next part!) let us log in into the machine, as you can see its from the `snmp-processes` output. This process check the host through a custom script and for this reason is seems that the binary need credentials (`daniel:HotelBabylon23`) but unfortunately for them they have leaked those in the parameters section, so now we just need to try them and ssh into the box!

![snmp-processses_credentials](/pandora_14.png)


# Exploitation

![ssh](/pandora_15.png)

The leaked credentials were good ! We succesfully logged in into the machine.

When looking into the machine we don't find our first flag strangly, but we can note that for the clearing tracks part the fact that our `.bash_history` is automatically redirect to `/dev/null` is pretty cool.

![ls_daniel](/pandora_16.png)

So we use `find` to search the file and we found it in the directory of matt.

![find](/pandora_17.png)

Unfortunately, when trying to see the file, we can't access it...

![cat_user_txt](/pandora_18.png)

When checking the permissions/ownership of the file, it's weird but for this box even the `user.txt` is own by `root`, so we'll need to escalate our privileges to access this one.

![ls_matt](/pandora_19.png)

Before going further we need to see if we have any sudo rights:

![sudo](/pandora_20.png)

OKay we dont, maybe some `SUID` program might help us ?

![find_suid](/pandora_21.png)

Bingo ! We found `pandora_backup` a `SUID` program, we even saw someone in the `snmp-processes` ouput list earlier used `tar` in the `tmp` directory and another process use pandora_backup, maybe we can try to do a relative path hijack!

First lets execute the script to see whats going on, on a normal use case:

![exec_pandora_backup](/pandora_22.png)

Mmmh weird. Let's see the rights/ownership of this file:

![file_pandora_backup](/pandora_23.png)

Really strange, same case that the `user.txt` file, the ownership is `root` and it's part of the group `matt`.

SO again a dead end, let's use a super tool that will automate this task (find ways to escalate privileges) for us.

![wget_les_local](/pandora_24.png)

What I just did above is downloading through `wget` `les.sh` on my local machine, a super script/tool. `les` stands for linux exploit suggester and to quote the definition on the github repo, '`LES` tool is designed to assist in detecting security deficiencies for given Linux kernel/Linux-based machine.' More info [here](https://github.com/mzet-/linux-exploit-suggester)!

After getting it on my local machine I need to send it to the target. So I just host the file on a custom `http` server with `python3` like this:

![host_les](/pandora_25.png)

Now on the target machine I do a `wget` like on my local machine to get the script:

![wget_les_target](/pandora_26.png)

And bingo we got the script on our target machine !

But why I did this extra step why not `wget` directly `github` from the target machine ? Well it's weird but when doing that the target machine won't resolve `raw.githubusercontent.com` and since I'm not the sysadmin for this target machine well I'll pass this all 'trying to resolve a DNS issue on the machine' thing since it can't be an internet problem...

Now I just need to make the script executable and check the results:

![chmodx_result_les](/pandora_27.png)

BINGO! We got something, it seems the machine is vulnerable to the `Pwnkit` exploit and it seems logic, first we saw in the `snmp-processes` output, the usage of `Polkit` daemon: `polkitd`,  when listing `SUID` programs we also notice two `SUID` programs, `/usr/bin/pkexec` and `/usr/lib/policykit-1/polkit-agent-helper-1`. Let's start with the Privilege Escalation phase since we found a way!

# Privilege Escalation

We're now almost certains how we're going to privesc, but before doing that what exactly is: `CVE-2021-4034`?

Well `CVE-2021-4034` commonly named `PwnKit` is a vulnerability involving a memory corruption. It was discovered in the pkexec command (install on all major Linux Distributions) it was announced on `January 25, 2022` but the vulnerability itself dates back to the original distribution from  `2009`. The vulnerability received a `CVSS` score of `7.8` ("High severity") reflecting serious factors involved in a possible exploit: unprivileged users can gain full `root` privileges locally, regardless of the underlying machine architecture or whether the polkit daemon is running or not.  If you want more info check [here](https://nvd.nist.gov/vuln/detail/CVE-2021-4034) !

###### Now it's exploit time!

Fortunately, when we used `les`, the script is good enough to give us the url (even if it wasn't provided by the script we could found it easily on `exploit-db`, `github`, `gitlab` etc... it's just time saving to implement features like that). Unfortunately, again we can't wget `from` the target machine directly since it didn't resolve `codeload.github.com`, so we'll do the same thing that we did for `les.sh`

First we download in local the exploit:

![wget_pwnkit_local](/pandora_28.png)

After checking what file type it is and unzip it like this:

![filetype_unzip](/pandora_29.png)

We need to `make` locally the exploit (compile with a lil +, in sort of since we're using a script a.k.a `makefile`) like this: 

![make](/pandora_30.png)

When it's done we can't use python to host the files like with `les` since it won't keep the attributes of the project, instead we will use `scp` to keep the integrity of the files and we will do it like this:

![scp](/pandora_31.png)

The files are indeed on the target machine!

![ls_pwnkit](/pandora_32.png)

Now we just need to enter into the folder and execute the exploit!

![l](/pandora_33.png)

BINGO we're `root`!

# Post-exploitation

In this phase, we'll be checking/talking about three things:

1. Completing our initial objective after privesc, a.k.a getting `user.txt` and `root.txt`

2. Talking about clearing tracks/gaining persistence on this machine

3. How to mitigate the attack?

###### Getting flags!
We get the `user.txt` like this:

![user.txt](/pandora_34.png)

We get the `root.txt` like this:

![root.txt](/pandora_35.png)


###### Clearing Tracks/ Getting Persistence

Like `daniel` or `matt` we can see that the `.bash_history` points to `/dev/null` so all commands that I run on either users or even root is not saved like we can see below:

![root_bash_history](/pandora_36.png)

- CLEARING TRACKS

This feature (`.bash_history` -> `/dev/null`) was setup on this box to not cheat if another user is exploiting the box too. But this allow us to execute any commands with any users or root and `.bash_history` will be empty, so no tracks here. If we want to implement this in a box that doesn't have this feature we just do this like that for a current session: `unset HISTFILE` or we add  `unset HISTFILE` at the end of the `.bashrc` (or the shell rc file the machine use) within the user or root directory we're dealing with. 
But clearing tracks is not just about `.bash_history`, we can do more things, like delete the `known_hosts` file  for `ssh` on all users/root With `shred` to be sure the file isn't recoverable. I  n `/var/log` we could also use `shred` but with particular options, that will be the most appropriate to really clear any traces, like `-v` to see the progress, `-f` to allow writing if necessary, `-z` to add a final overwrite with zeros to hide the shredding, last option would be the `-u` to deallocate and remove file after overwriting. The final command would look like this: `shred -vfzu /var/log/auth.log` (`auth.log` -> Contains system authorization information, including user logins and authentication mechanism that were used), this can also be used in `/var/log/kern.log` since it logs all the kernel informations so pretty important too. We should use it on `/var/log/messages` since it keeps tracks of logging kern, auth, daemon event this would be useful since snmpd logged our activity when we execute our scripts. Last thing we could delete will be `/var/log/wtmp` since they keep logs of all connections/ disconections to the system.

- GAINING PERSISTENCE

To gain persistence multiple options could be used, first we can upload our public key to the target (`id_rsa.pub`) into the `/home/user/.ssh` folder and add it to the `authorized_keys` file of one of the user or `root` (with further configurations in `/etc/ssh/sshd_config` if `root`) then login from local machine with only  username and ip. Since the exploit is `LPE` you can just hide it, create a special directory or put it in `tmp`, so when you reporudce the steps, you just log in from the user and get root access through the `LPE`. The second option create much noise but is also reliable, you create a cron job and execute the exploit every x minutes/hours etc... Through a work of pipe with a script or by modifying the exploit (don't execute a shell but a python reverse shell for example) you get a connection right back at your terminal with a root access.

###### Mitigate the attack?

Well for this part, we'll need to rerun our pentest scenario and at each stage propose a remediation.

OK first thing we did was a `ping` test, if we can avoid this to be more stealth it would be a good choice, but on the other hand for a snmp network based it's pretty important to check connection to the agents so it's your call. How do we block the `ping`, well we add an `iptables` rule -> you can see more details [here](https://www.tecmint.com/block-ping-icmp-requests-to-linux/)!

Now after the `ping`, was the scanning part. Wether it is the first one (discovered `22/80` port with a `SYN` based scan) or the second one (discovered the `161` port with a `UDP` based port). They were both detectable by `IDS` since I didnt use special options like `-f`/ `-ff` (fragmented packets), `-sI` (idle scan) or also `-T<0-5>` (timing template)! So from my side I'll be more stealth by using these options but from the server side, implementing an `IDS` (since there wasnt one it's just in real world scenario with same scan I would be detected) and strict firewall rules would be great especially for `22` and `161` (since `80` is for serving website of course...) and be even more strict especially with `161`. Since it's using `net-snmp` as a service, we know that the protocol only communicate via a client-server relation in sort of (the client are agents that runs on devices, in this case a linux machine and the server is called a manager in the context of `SNMP` network based, it can also be a middle man like a master agent to forward the commands send by the manager to the list of agents under him and then communicate the results to the manager).
To get things clear he's a map of how it works (typically):

![snmp_map](/pandora_37.png)

 Since the relation is pretty similar to a client-server one (if we forget about the potential middle man a.k.a `Master Agent`) configuring the firewall by blocking every ip other than the `Manager` or `Master Agent` for the `Agent` (since the machine we were exploiting is an `Agent` type) would be great. To setup a simple firewall (a.k.a `UFW` or Uncomplicated FireWall) you can check -> [here](https://www.maketecheasier.com/how-to-set-up-firewall-linux/). With that new policy, it lets us block all possibilities for another device on the network to enumerate through this port all the infos we found earlier. But if we continue our scenario it's not enough. Imagine for x/y/z reasons we found a way to access the `Manager` or `Master Agent`, we could enumerate infos of our `Agent` target right ? Well in this case, the confidentiality has to be preserved, because if we had access to the machine, we could see all the credentials we saw earlier. It's the worst situation, they're in plaintext hard coded in the parameters of the command we could see. In this case the firewall can be strict as possible if we access a `Manager` we could leaks those credentials, so to protect those informations, we have two options (if this script in question has to use password absolutely) we have first, the option to get those credentials from another file, that has rights and is ownership protected and accessible through that script but without leaking those credentials. The second option can be to handle those credentials using environment variables, it would be not as good as the first option but on the other way if we can only take a glance from the outside it's still better to see `$PASS` rather then `HotelBabylon23` (Warning in any case, if someone has access to the machine, the credential can be leaked by using a simple `echo $PASS` for example, this should only be a temporary solution during a certain time to remediate the attack,not some long term policy!)

Since we got those credentials via the port `161`, even if we access the machine in another way, there's still the most problematic issue, the privilege escalation vulnerability is still presents on the machine, I'm talking about `CVE-2021-4034` a.k.a `PwnKit`, this should be resolve as soon as possible by an automated way via updating the machine's system directly (check [here](https://www.thesecmaster.com/how-to-fix-the-polkit-privilege-escalation-vulnerability-cve-2021-4034/)), this is possible for major distro like Ubuntu, RedHat, Debian, or SUSE. But if you can't do that or like this machine it's a "custom" distro based of Ubuntu for example you should try to follow this notice produced by Canonical just [here](https://ubuntu.com/security/notices/USN-5252-1). You can also do it manually if you don't have an internet access to this machine or you want to do it yourself, those links should help ->
- [https://access.redhat.com/security/vulnerabilities/RHSB-2022-001](https://access.redhat.com/security/vulnerabilities/RHSB-2022-001)
- [https://gitlab.freedesktop.org/polkit/polkit/-/commit/a2bf5c9c83b6ae46cbd5c779d3055bff81ded683](https://gitlab.freedesktop.org/polkit/polkit/-/commit/a2bf5c9c83b6ae46cbd5c779d3055bff81ded683)

Hope this write-up will help you to get a full view of the machine, from the Information Gathering phase to the mitigation of this attack!

> Fas est et ab hoste doceri.