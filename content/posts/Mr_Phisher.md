+++
title = "Mr. Phisher"
date = "2022-06-17T20:00:19-04:00"
author = "z0ne"
tags = ["THM", "Blue Team", "Macros"]
+++

# Introduction

This post will give you an inside of one of the most common way for attackers to get inside a network.

Now let's start with the instructions.

# Instructions

![instructions_0](/mr_phisher_0.png)

First we see this: `I received a suspicious email with a very weird looking attachment. It keeps on asking me to "enable macros". What are those?`.

It tells us immediately that we will be dealing with macros, probably on some kind of Office document so that's the root of the challenge.

![instructions_1](/mr_phisher_1.png)

There we can see instructions on how to fire up the vm and of course a link for more phishing contents to learn!

I put you a link for this room [here](https://tryhackme.com/room/mrphisher) if you want to follow along.

# Opening our file 

After we open the VM we can find our file right there:

![file](/mr_phisher_2.png)

If we open it, we got something interesting:

![macros_notification](/mr_phisher_3.png)

This notifications tells that the doc contains macros and therefore alerts us that those could contains viruses.

The second thing that we can notice is that the execution of macros has been disabled due to the current state of the macro security settings in Libre Office.

After opening the file we see that the content is not important at all:

![content_of_the_file](/mr_phisher_4.png)

# Accessing the macros

Now we're going to access the macros by going into `Tools -> Macros -> Edit Macros` just like this:

![accessing_macros](/mr_phisher_5.gif)

In the Macros tab we're going to check one specific object in the catalog, the path will be: `MrPhisher.docm -> Project -> Modules -> NewMacros -> Format`

![Checking_our_object](/mr_phisher_6.gif)

Now let's start the analysis of the script.

# Analysis of the script

Ok first, the script is written in `VBA`, you can find reference [here](https://docs.microsoft.com/en-us/office/vba/api/overview/)

So what is it ? Well if we take the definition from the microsoft documentation, Office Visual Basic for Applications (VBA) is an event-driven programming language that enables you to extend Office applications. In short you can execute script through Office files with `VBA` scripts.

Now let's start the analysis by explaining what each line is doing.

First I put the whole script here:

```vba
Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Sub Format()
    Dim a()
    Dim b As String
    a = Array(102, 109, 99, 100, 127, 100, 53, 62, 105, 57, 61, 106, 62, 62, 55, 110, 113, 114, 118, 39, 36, 118, 47, 35, 32, 125, 34, 46, 46, 124, 43, 124, 25, 71, 26, 71, 21, 88)
    For i = 0 To UBound(a)
        b = b & Chr(a(i) Xor i)
    Next
End Sub
```

1. Ok so the first line isn't that interesting but through the keyword `Rem` it's creating a comment related to `VBAModule` (more info [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/rem-statement)).

2. The keyword `Option`, through the parameters `{1|0}` let's us enable/disable VBA support in LibreOffice (more info: [here](https://help.libreoffice.org/latest/he/text/sbasic/shared/03103350.html))

3. The keyword `Sub` let's us declare the name, arguments, and code that form the body of a Sub procedure. (more info: [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/sub-statement))

4. The keyword `Dim` let's us declare variables and allocates storage space. (more info [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/dim-statement))  

5. Same as the fourth line but the `b` variable is a `String`

6. After declaring the `a` variable, we're using it through the call of the `Array()` function. (more info [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/array-function))

7. On this line we're using a `For` loop with an i variable initialized at 0. (more info: [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/fornext-statement)) The function `Ubound()` gets the length of an array. (more info [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/ubound-function)) The for loop will go from `0` to the length of the `a` array (`38`).

8. This line start by giving a value to the `b` variable. The first thing we notice is the `&` operator (more info: [here](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/operators/concatenation-operator)). This operator generates a string concatenation of two expressions. In our case it's the former value of `b` for the first expression. The second expression is using the `Chr()` function to return a `String` containing the character associated with the specified character code. (more info: [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/chr-function)). The argument provided to the `Chr()` function has a `Xor` operation in it with two variables. (more info: [here](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/operators/xor-operator)). The first one is the value of the current element of the array, it's written like this: `a(i)`. The second one is the `i` variable that's holding the current number of execution of the loop.

9. This line is pretty simple it's just the keyword `Next` that let's the for loop keep going to the next instruction.

10. The last line `End` the procedure. (more info: [here](https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/end-statement)) I hope you did enjoy this post and it gave you an insight of how you can do some static/dynamic analysis from an infected doc!
Perfect now that we did a static analysis of the script, we can execute the `VBA` script in a secure environment to see what's happening when the script is running.

![trying_to_run_script](/mr_phisher_7.gif)

But wait, we have a problem...

**We can't RUN the script!**

Well again we have to go on a certain path to remediate this problem. So we should go there: `Tools -> Options -> Security -> Macro Security...`

![lower_the_security_level](/mr_phisher_8.gif)

After this you should close the file and reopen for the settings to be effective.

![run_the_script](/mr_phisher_9.gif)

But wait, we don't have any problems to run the script now but we don't see anything either...

Maybe we should use the `Step Over` feature to see exactly how the instructions are executed.

![execution_order](/mr_phisher_10.gif)

With the arrow on the left (close to the numbers for each line of the script), we can see what instructions is executed at a given time, of course when going through the loop we have to click a lot on the button to get to the "after for loop", but what will be interesting for us is the after, maybe we could put a breakpoint when we arrive on the `End Sub` instruction.

Of course I'm not going to let you suffer and watch all over again so I'll share a new gif at the moment where we hit that last instruction.

![last_instruction](/mr_phisher_11.gif)

As you can see we put our beakpoint exactly where we wanted. So if we execute the script the arrow will stop at the `End` instruction thanks to the breakpoint. A quick side note, the window on the bottom right is pretty cool for debugging since it tells us the call made to the script, for example the `0: Format` is the call at the opening of the procedure named `Format()` on the third line.

The other window that will be very useful to us is the one on the bottom left of the screen. It let's us watch variables and their value at a given time. Since we know the string we need to decode is hold in the `b` variable we should check it like this:

![watch_b](/mr_phisher_12.gif)

Now that we have everything ready, we just rerun our script and see what we have on the bottom left of the screen! :

![flag_through_watch](/mr_phisher_13.gif)

And there you have it! our flag through dynamic analysis! The flag is: `flag{a39a07a239aacd40c948d852a5c9f8d1}`.

We could totally stop there, but as I saw other writeups, especially this [one](https://github.com/KyootyBella/THM-Writeups/blob/main/Mr.%20Phisher/README.md) (you can find it through the link or on the Writeups page of the room).

There is another way to solve this and it's done by taking the data we found on the array and "copying" the script in our favorite language to output the flag in local on our machine!

[@KyootyBella](https://github.com/KyootyBella) (the author of the first write-up I saw with the tip of copying data and create a script) has done this task in python. Since I don't want to do exactly like this and prefer go, I'll write the script in go!

# Creating the script!

The original script:

```vba
Sub Format()
    Dim a()
    Dim b As String
    a = Array(102, 109, 99, 100, 127, 100, 53, 62, 105, 57, 61, 106, 62, 62, 55, 110, 113, 114, 118, 39, 36, 118, 47, 35, 32, 125, 34, 46, 46, 124, 43, 124, 25, 71, 26, 71, 21, 88)
    For i = 0 To UBound(a)
        b = b & Chr(a(i) Xor i)
    Next
End Sub
```

My go version of it:

```go
package main

import "fmt"

func main() {
        a:= []int{102, 109, 99, 100, 127, 100, 53, 62, 105, 57, 61, 106, 62, 62, 55, 110, 113, 114, 118, 39, 36, 118, 47, 35, 32, 125, 34, 46, 46, 124, 43, 124, 25, 71, 26, 71, 21, 88}
        b:= ""
        for i := 0; i < len(a); i++ {
                b = b + string(a[i] ^ i)
        }
         
        fmt.Println(b)
}
```

# Execution steps

![execution_steps](/mr_phisher_14.gif)

Perfect!

Again our program gave us the good flag since we have the same result from two different manners.

Let's check if our flag is good then!

# Final

![final](/mr_phisher_15.gif)

YES! Our flag is good! 

I hope you did enjoy this post and it gave you an insight of how you can do some static/dynamic analysis from an infected doc!

> “Il est bien plus beau de savoir quelque chose de tout que de savoir tout d'une chose.” - Blaise Pascal
